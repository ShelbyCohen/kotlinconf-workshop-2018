package full.stack.kotlin.services

import full.stack.kotlin.model.Movie
import full.stack.kotlin.model.MoviePlusCastSize
import full.stack.kotlin.repositories.MovieRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@RestController
@RequestMapping("/movies")
class Neo4JMoviesService(val repo: MovieRepository) {
    @GetMapping("/byTitle/{title}")
    fun moviesByTitle(@PathVariable("title") title: String): Flux<Movie> {
        return Flux.fromIterable(repo.findByTitle(title))
    }

    @GetMapping("/byPhrase/{phrase}")
    fun moviesByPhrase(@PathVariable("phrase") phrase: String): Flux<Movie> {
        return Flux.fromIterable(repo.findByTitleContaining(phrase))
    }

    @GetMapping("/byActorId/{actorId}")
    fun moviesByActorId(@PathVariable("actorId") actorId: String): Flux<Movie> {
        return Flux.fromIterable(repo.findByActorId(actorId))
    }

    @GetMapping("/byFilmography/{minMovies}")
    fun moviesByActorId(@PathVariable("minMovies") minMovies: Long): Flux<Movie> {
        return Flux.fromIterable(repo.findByFilmography(minMovies))
    }

    @GetMapping("/byTitleAddCastSize/{title}")
    fun moviesByTitlePlusCaseSize(@PathVariable("title") title: String): Flux<MoviePlusCastSize> {
        return Flux.fromIterable(repo.findPlusCastSize(title))
    }
}