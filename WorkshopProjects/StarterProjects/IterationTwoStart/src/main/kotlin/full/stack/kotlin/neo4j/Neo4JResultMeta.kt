package full.stack.kotlin.neo4j

class Neo4JResultMeta {
    var id: String? = null
    var type: String? = null
    var isDeleted: Boolean = false
}
