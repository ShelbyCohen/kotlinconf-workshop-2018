package full.stack.kotlin.model

class Movie {
    var studio: String? = null
    var releaseDate: String? = null
    var imdbId: String? = null
    var runtime: String? = null
    var description: String? = null
    var language: String? = null
    var title: String? = null
    var version: String? = null
    var trailer: String? = null
    var imageUrl: String? = null
    var genre: String? = null
    var tagline: String? = null
    var lastModified: String? = null
    var id: String? = null
    var homepage: String? = null
}
