package full.stack.kotlin.services

import full.stack.kotlin.clients.Neo4JRestClient
import full.stack.kotlin.model.Movie
import full.stack.kotlin.neo4j.Neo4JResponse
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@RestController
class Neo4JMoviesService(private val client: Neo4JRestClient) {

    @GetMapping(value = ["/someMovies/{limit}"], produces = ["application/json"])
    internal fun someMovies(@PathVariable("limit") limit: Int): Flux<Movie> {
        val typeRef = object : ParameterizedTypeReference<Neo4JResponse<Movie>>() {}
        val query = "MATCH (m:Movie) RETURN m LIMIT $limit"

        return client.runCypherQuery(query, typeRef) ?: Flux.empty()
    }

    @GetMapping(value = ["/moviesByActor/{id}"], produces = ["application/json"])
    internal fun allMoviesByActor(@PathVariable("id") id: Int): ResponseEntity<Flux<Movie>> {
        val typeRef = object : ParameterizedTypeReference<Neo4JResponse<Movie>>() {}
        val query = "MATCH (m:Movie)<-[:ACTS_IN]-(a:Actor {id:\"$id\"}) RETURN m"

        val result = client.runCypherQuery(query, typeRef)
        return if (result != null) {
            ResponseEntity.ok(result)
        } else {
            ResponseEntity.noContent().build()
        }
    }
}
