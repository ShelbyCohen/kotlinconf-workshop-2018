package full.stack.kotlin

import org.junit.Assert.assertTrue
import org.junit.Assert.assertNotNull
import full.stack.kotlin.model.Movie
import org.junit.Test
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient

class MoviesServiceTest {
    private val client = WebTestClient
            .bindToServer()
            .baseUrl("http://localhost:8080")
            .build()

    @Test
    fun specifiedNumberOfMoviesCanBeFound() {
        val numMovies = 20
        this.client.get().uri("/someMovies/$numMovies")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().is2xxSuccessful
                .expectBodyList(Movie::class.java)
                .hasSize(numMovies)
    }

    @Test
    fun moviesCanBeFoundByActorId() {
        fun assertMovieExists(movies: List<Movie>, title: String) {
            val exists = movies.any { it.title == title }
            assertTrue("No movie called $title", exists)
        }

        val typeRef = object : ParameterizedTypeReference<List<Movie>>() {}
        val actorId = 3
        this.client.get().uri("/moviesByActor/$actorId")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectBody<List<Movie>>(typeRef)
                .returnResult()
                .apply {
                    assertNotNull(responseBody)
                    val movies = responseBody ?: emptyList()
                    assertMovieExists(movies, "Star Wars: Episode IV - A New Hope")
                    assertMovieExists(movies, "Indiana Jones and the Last Crusade")
                }
    }
}