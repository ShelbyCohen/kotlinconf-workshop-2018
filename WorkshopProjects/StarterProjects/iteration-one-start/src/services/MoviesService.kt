package services

import model.Actor
import model.Movie

interface MoviesService {
    fun loadAllMovies(start: Int, limit: Int, handler : (List<Movie>) -> Unit)
    fun loadMoviesByActorId(actorId: Int, start: Int, limit: Int, handler : (List<Movie>) -> Unit)
    fun loadMoviesByActorName(actorName: String, start: Int, limit: Int, handler : (List<Movie>) -> Unit)
    fun loadActorsByCountOfFilmsMade(filmLimit: Int, handler : (List<Actor>) -> Unit)
}