package services.neo4j

interface Neo4JResultRow<T> {
    val row: Array<T>
    val meta: Array<String>
}