package services.neo4j

data class Neo4JStatementList(var statements: List<Neo4JStatement>)