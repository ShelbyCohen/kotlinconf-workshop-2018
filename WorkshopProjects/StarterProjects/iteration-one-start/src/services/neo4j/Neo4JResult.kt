package services.neo4j

interface Neo4JResult<T> {
    val columns: Array<String>
    val data: Array<Neo4JResultRow<T>>
}