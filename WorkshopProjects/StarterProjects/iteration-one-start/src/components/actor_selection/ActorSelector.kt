package components.actor_selection

import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLInputElement
import react.RBuilder
import react.RComponent
import react.RHandler
import react.dom.*
import react.setState

fun RBuilder.actorSelection(handler: RHandler<ActorSelectionProps>) = child(ActorSelection::class, handler)

class ActorSelection(props: ActorSelectionProps) : RComponent<ActorSelectionProps, ActorSelectionState>(props) {
    init {
        state.actorName = "Harrison Ford"
    }

    override fun RBuilder.render() {
        form(classes = "col-12") {
            span(classes="mr-2") {
                label(classes = "mr-2") { +"Actors Name:" }
                input(type = InputType.text) {
                    attrs {
                        value = state.actorName
                        onChangeFunction = { event ->
                            val textBox = event.target as HTMLInputElement
                            setState { actorName = textBox.value }
                        }
                    }
                }
                input(type = InputType.button) {
                    attrs {
                        value = "Find Films"
                        onClickFunction = { props.actorChosenHandler(state.actorName) }
                    }
                }
            }
        }
    }
}