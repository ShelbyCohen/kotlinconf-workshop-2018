package components.actor_selection

import react.RState

interface ActorSelectionState: RState {
    var actorName: String
}