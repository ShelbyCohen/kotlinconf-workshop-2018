package components.actor_selection

import react.RProps
import services.MoviesService

class ActorSelectionProps(var actorChosenHandler: (String) -> Unit): RProps