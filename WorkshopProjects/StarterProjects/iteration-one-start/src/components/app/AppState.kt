package components.app

import react.RState
import model.Movie

interface AppState : RState {
    var currentMovies: List<Movie>
    var currentActorName: String
    var endOfData: Boolean
    var start: Int
    var limit: Int
}