package components.app

import components.actor_selection.actorSelection
import components.app.App.Direction.BACKWARD
import components.app.App.Direction.FORWARD
import components.movie_table.movieTable
import components.nav_bar.navBar
import react.*
import react.dom.*
import services.MoviesServiceViaAxiosAndNeo4J


class App : RComponent<AppProps, AppState>() {
    private enum class Direction { FIRST_USE, FORWARD, BACKWARD }
    private val service = MoviesServiceViaAxiosAndNeo4J("neo4j","password")

    init {
        state.currentMovies = emptyList()
        resetSearchCriteria("",0,10)
    }

    private fun resetSearchCriteria(name: String, start: Int, limit: Int) {
        state.currentActorName = name
        state.endOfData = false
        state.start = start
        state.limit = limit
    }

    override fun RBuilder.render() {
        div(classes = "container") {
            div(classes = "row") {
                h2(classes = "col-8 text-center") { +"Movie SPA Iteration One" }
            }
            div(classes = "row mt-3") {
                actorSelection {
                    attrs {
                        actorChosenHandler = { name ->
                            resetSearchCriteria(name,0,10)
                            loadMovies(Direction.FIRST_USE)
                        }
                    }
                }
            }
            if(state.currentMovies.isNotEmpty()) {
                div(classes = "row mt-3") {
                    div(classes = "col-2") {
                        navBar {
                            attrs {
                                backwardHandler = ::loadMoviesBackward
                                forwardHandler = ::loadMoviesForward
                            }
                        }
                    }
                }
                div(classes = "row mt-1") {
                    div(classes = "col-9") {
                        movieTable {
                            attrs {
                                movies = state.currentMovies
                                selectedHandler = { console.log("Movie '${it.title}' Selected...") }
                            }
                        }
                    }
                    div(classes = "col-3") {
                        b { +"Movie Details Go Here" }
                    }
                }
            }
        }
    }

    private fun loadMovies(direction: Direction) {
        adjustStartPosition(direction)
        loadMoreMovies()
    }

    private fun loadMoreMovies() {
        service.loadMoviesByActorName(
                state.currentActorName,
                state.start,
                state.limit) { data ->
            if (data.isNotEmpty()) {
                setState { currentMovies = data }
            } else {
                handleEndOfData()
            }
        }
    }

    private fun adjustStartPosition(direction: Direction) {
        when (direction) {
            FORWARD -> {
                if (!state.endOfData) {
                    state.start += state.limit
                }
            }
            BACKWARD -> {
                state.endOfData = false
                val newStart = state.start - state.limit
                state.start = if (newStart > 0) newStart else 0
            }
        }
    }

    private fun handleEndOfData() {
        state.endOfData = true
        state.start -= state.limit
    }

    private fun loadMoviesForward()  = loadMovies(FORWARD)
    private fun loadMoviesBackward() = loadMovies(BACKWARD)
}

fun RBuilder.app() = child(App::class) {}
