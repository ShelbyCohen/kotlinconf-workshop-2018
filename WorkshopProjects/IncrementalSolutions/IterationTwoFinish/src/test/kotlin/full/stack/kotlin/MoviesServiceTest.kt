package full.stack.kotlin

import full.stack.kotlin.model.Movie
import org.junit.Assert.assertTrue
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient

class MoviesServiceTest {
    private val client = WebTestClient
            .bindToServer()
            .baseUrl("http://localhost:8080")
            .build()

    @Test
    fun specifiedNumberOfMoviesCanBeFound() {
        val numMovies = 20
        this.client.get().uri("/someMovies/$numMovies")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().is2xxSuccessful
                .expectBodyList(Movie::class.java)
                .hasSize(numMovies)
    }

    @Test
    fun moviesCanBeFoundByActorId() {
        fun assertMovieExists(movies: List<Movie>, title: String) {
            val exists = movies.any { it.title == title }
            assertTrue("No movie called $title", exists)
        }

        val typeRef = object : ParameterizedTypeReference<List<Movie>>() {}
        val actorId = 3
        this.client.get().uri("/moviesByActor/$actorId")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectBody<List<Movie>>(typeRef)
                .returnResult()
                .apply {
                    assertNotNull(responseBody)
                    val movies = responseBody ?: emptyList()
                    assertMovieExists(movies, "Star Wars: Episode IV - A New Hope")
                    assertMovieExists(movies, "Indiana Jones and the Last Crusade")
                }
    }

    @Test
    fun moviesCanBeFoundByActorName() {
        fun assertMovieExists(movies: List<Movie>, title: String) {
            val exists = movies.any { it.title == title }
            assertTrue("No movie called $title", exists)
        }

        val typeRef = object : ParameterizedTypeReference<List<Movie>>() {}
        val actorName = "Harrison Ford"
        this.client.get().uri("/moviesByActorName/$actorName")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectBody<List<Movie>>(typeRef)
                .returnResult()
                .apply {
                    assertNotNull(responseBody)
                    val movies = responseBody ?: emptyList()
                    assertMovieExists(movies, "Star Wars: Episode IV - A New Hope")
                    assertMovieExists(movies, "Indiana Jones and the Last Crusade")
                }
    }

    @Test
    fun moviesCanBeFoundByDirectorName() {
        fun assertMovieExists(movies: List<Movie>, title: String) {
            val exists = movies.any { it.title == title }
            assertTrue("No movie called $title", exists)
        }

        val typeRef = object : ParameterizedTypeReference<List<Movie>>() {}
        val directorName = "Leonard Nimoy"
        this.client.get().uri("/moviesByDirectorName/$directorName")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectBody<List<Movie>>(typeRef)
                .returnResult()
                .apply {
                    assertNotNull(responseBody)
                    val movies = responseBody ?: emptyList()
                    assertEquals(4, movies.size)
                    assertMovieExists(movies, "Three Men and a Baby")
                    assertMovieExists(movies, "Holy Matrimony")
                    assertMovieExists(movies, "Star Trek III: The Search for Spock")
                    assertMovieExists(movies, "Star Trek IV: The Voyage Home")
                }
    }

    @Test
    fun moviesCanBeFoundByGenre() {
        val genre = "Animation"
        val numMovies = 426
        this.client.get().uri("/moviesByGenre/$genre")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().is2xxSuccessful
                .expectBodyList(Movie::class.java)
                .hasSize(numMovies)
    }
}