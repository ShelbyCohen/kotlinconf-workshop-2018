package full.stack.kotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class IterationTwoStartApplication

fun main(args: Array<String>) {
    runApplication<IterationTwoStartApplication>(*args)
}
