package full.stack.kotlin.neo4j

class Neo4JResult<T> {
    var columns: List<String>? = null
    var data: List<Neo4JResultRow<T>>? = null
}