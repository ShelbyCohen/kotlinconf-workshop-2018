package full.stack.kotlin.clients

import full.stack.kotlin.neo4j.Neo4JResponse
import full.stack.kotlin.neo4j.Neo4JStatement
import full.stack.kotlin.neo4j.Neo4JStatementList
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.ReactiveHttpOutputMessage
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserter
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Flux.fromIterable
import reactor.core.publisher.Mono
import java.util.Arrays

@Component
class Neo4JRestClient(private val client: WebClient) {
    fun <T> runCypherQuery(query: String, typeRef: ParameterizedTypeReference<Neo4JResponse<T>>): Flux<T>? {
        val mono = client.post()
                .body(bodyFromQuery(query))
                .exchange()

        return extractData(mono, typeRef)
    }

    private fun <T> extractData(mono: Mono<ClientResponse>, typeRef: ParameterizedTypeReference<Neo4JResponse<T>>): Flux<T>? {
        return mono.flatMapMany { response ->
            if (response.statusCode().is2xxSuccessful) {
                parseResponseBody(response, typeRef)
            } else {
                null
            }
        }
    }

    private fun <T> parseResponseBody(response: ClientResponse, typeRef: ParameterizedTypeReference<Neo4JResponse<T>>): Flux<T> {
        return response.bodyToFlux(typeRef)
                .map { it.results }
                .flatMap { fromIterable(it.orEmpty()) }
                .flatMap { fromIterable(it.data!!) }
                .flatMap { fromIterable<T>(it.row!!) }
    }

    fun bodyFromQuery(query: String): BodyInserter<Neo4JStatementList, ReactiveHttpOutputMessage> {
        val statement = Neo4JStatement(query)
        val statementList = Neo4JStatementList(Arrays.asList(statement))
        return BodyInserters.fromObject(statementList)
    }
}