package full.stack.kotlin

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import java.util.Base64.getEncoder

@Configuration
class IterationTwoConfig(private val env: Environment) {
    private fun buildAuthorization(): String {
        val username = env.getProperty("instil.neo4j.user")
        val password = env.getProperty("instil.neo4j.password")
        val token = "$username:$password"
        val encodedToken = getEncoder().encodeToString(token.toByteArray())
        return "Basic $encodedToken"
    }

    @Bean
    fun clientBuilder(): WebClient {
        return WebClient.builder()
                .baseUrl("http://localhost:7474/db/data/transaction/commit")
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, buildAuthorization())
                .build()
    }
}
