package services.neo4j

interface Neo4JResultList<T> {
    val results: Array<Neo4JResult<T>>
    val errors: Array<String>
}

