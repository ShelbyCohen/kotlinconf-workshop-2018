package services

import kotlinext.js.jsObject
import model.Actor
import services.axios.AxiosConfigSettings
import services.axios.AxiosResponse
import model.Movie
import services.neo4j.Neo4JResultList
import services.neo4j.Neo4JStatement
import services.neo4j.Neo4JStatementList
import kotlin.js.Json
import kotlin.js.Promise
import kotlin.js.json

@JsModule("axios")
external fun <T> axios(config: AxiosConfigSettings): Promise<AxiosResponse<T>>

class MoviesServiceViaAxiosAndNeo4J(private val username: String, private val password: String) : MoviesService {
    override fun loadMoviesByActorId(actorId: Int, start: Int, limit: Int, handler: (List<Movie>) -> Unit) {
        val query = "MATCH (m:Movie)<-[:ACTS_IN]-(a:Actor {id:\"$actorId\"}) RETURN m SKIP $start LIMIT $limit"
        sendQuery(query, handler)
    }

    override fun loadMoviesByActorName(actorName: String, start: Int, limit: Int, handler: (List<Movie>) -> Unit) {
        val query = "MATCH (m:Movie)<-[:ACTS_IN]-(a:Actor) WHERE a.name STARTS WITH \"$actorName\" RETURN m SKIP $start LIMIT $limit"
        sendQuery(query, handler)
    }

    override fun loadAllMovies(start: Int, limit: Int, handler: (List<Movie>) -> Unit) {
        val query = "MATCH (m:Movie) RETURN m SKIP $start LIMIT $limit"
        sendQuery(query, handler)
    }

    override fun loadActorsByCountOfFilmsMade(filmLimit: Int, handler: (List<Actor>) -> Unit) {
        val query = "MATCH (a:Actor)-[:ACTS_IN]->(m:Movie) WITH a, collect(m) AS filmograpy WHERE size(filmograpy) >= $filmLimit RETURN a"
        sendQuery(query, handler)
    }

    private fun <T> sendQuery(actorsByNumOfMovies: String, handler: (List<T>) -> Unit) {
        axios<Neo4JResultList<T>>(settings(actorsByNumOfMovies)).then { response ->
            handler(extractData(response.data))
        }
    }

    private fun settings(query: String): AxiosConfigSettings {
        return jsObject {
            url = "http://localhost:7474/db/data/transaction/commit"
            method = "POST"
            headers = buildHeaders()
            data = buildQuery(query)
        }
    }

    private fun buildHeaders(): Json {
        val token = "$username:$password"
        val encodedToken = js("btoa(token)")
        return json(
                "Accept" to "application/json",
                "Content-Type" to "application/json",
                "Authorization" to "Basic $encodedToken")
    }

    private fun <T> extractData(input: Neo4JResultList<T>): List<T> {
        return input.results[0].data.map { it.row[0] }
    }

    private fun buildQuery(query: String): Neo4JStatementList {
        return Neo4JStatementList(listOf(Neo4JStatement(query)))
    }
}