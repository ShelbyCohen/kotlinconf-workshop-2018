package full.stack.kotlin

import full.stack.kotlin.model.Actor
import full.stack.kotlin.model.Movie
import org.junit.Assert.assertTrue
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient

class Neo4JMoviesServiceTest {
    private val client = WebTestClient
            .bindToServer()
            .baseUrl("http://localhost:8080/movies")
            .build()

    private val typeRef = object : ParameterizedTypeReference<List<Movie>>() {}

    fun assertMovieExists(movies: List<Movie>, title: String) {
        val exists = movies.any { it.title == title }
        assertTrue("No movie called $title", exists)
    }

    @Test
    fun actorsCanBeFoundByFilmography() {
        fun assertContains(names: List<String?>, name: String) {
            assertTrue("Can't find $name", names.contains(name))
        }

        val minMovies = 50
        this.client.get().uri("/byFilmography/$minMovies")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().is2xxSuccessful
                .expectBodyList(Actor::class.java)
                .hasSize(6)
                .returnResult()
                .responseBody
                .orEmpty()
                .map { it.name }
                .apply {
                    assertContains(this, "Samuel L. Jackson")
                    assertContains(this, "Gérard Depardieu")
                    assertContains(this, "Claude Jade")
                    assertContains(this, "Marisa Mell")
                    assertContains(this, "Robert De Niro")
                    assertContains(this, "Armin Mueller-Stahl")
                }
    }

    @Test
    fun moviesCanBeFoundByActorName() {
        val actorName = "Robert De Niro"
        val skip = 0
        val limit = 20
        this.client.get().uri("/byActorName/$actorName/$skip/$limit")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().is2xxSuccessful
                .expectBody<List<Movie>>(typeRef)
                .returnResult()
                .apply {
                    assertNotNull(responseBody)
                    val movies = responseBody ?: emptyList()
                    assertEquals(limit, movies.size)
                    assertMovieExists(movies, "Men of Honor")
                    assertMovieExists(movies, "The Mission")
                    assertMovieExists(movies, "Analyze That")
                }
    }
}
