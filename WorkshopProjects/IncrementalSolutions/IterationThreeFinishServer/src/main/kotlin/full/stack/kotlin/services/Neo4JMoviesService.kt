package full.stack.kotlin.services

import full.stack.kotlin.model.Actor
import full.stack.kotlin.model.Movie
import full.stack.kotlin.repositories.MovieRepository
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.PathVariable
import reactor.core.publisher.Flux

@CrossOrigin
@RestController
@RequestMapping("/movies")
class Neo4JMoviesService(val repo: MovieRepository) {
    @GetMapping("/byFilmography/{minMovies}")
    fun moviesByActorId(@PathVariable("minMovies") minMovies: Long): Flux<Actor> {
        return Flux.fromIterable(repo.findActorsByFilmography(minMovies))
    }

    @GetMapping("/byActorName/{name}/{skip}/{limit}")
    fun moviesByActorName(
        @PathVariable("name") name: String,
        @PathVariable("skip") skip: Long,
        @PathVariable("limit") limit: Long
    ): Flux<Movie> {
        return Flux.fromIterable(repo.findByActorName(name, skip, limit))
    }
}