package full.stack.kotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class IterationThreeFinishServerApplication

fun main(args: Array<String>) {
    runApplication<IterationThreeFinishServerApplication>(*args)
}
