package full.stack.kotlin

import full.stack.kotlin.services.MoviesWebSocketHandler
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter
import java.util.Base64.getEncoder

@Configuration
class WebSocketsDemoConfig(val context: ApplicationContext, private val env: Environment) {
    private fun buildAuthorization(): String {
        val username = env.getProperty("instil.neo4j.user")
        val password = env.getProperty("instil.neo4j.password")
        val token = "$username:$password"
        val encodedToken = getEncoder().encodeToString(token.toByteArray())
        return "Basic $encodedToken"
    }

    @Bean
    fun clientBuilder(): WebClient {
        return WebClient.builder()
                .baseUrl("http://localhost:7474/db/data/transaction/commit")
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, buildAuthorization())
                .build()
    }

    @Bean
    fun adapter() = WebSocketHandlerAdapter()

    @Bean
    fun mapping(): HandlerMapping {
        val map = mutableMapOf<String, Any>()
        map["/movies"] = context.getBean(MoviesWebSocketHandler::class.java)
        val mapping = SimpleUrlHandlerMapping()
        mapping.urlMap = map
        mapping.order = 10
        return mapping
    }
}