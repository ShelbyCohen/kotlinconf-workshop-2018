package full.stack.kotlin.neo4j

class Neo4JResponse<T> {
    var results: List<Neo4JResult<T>>? = null
    var errors: List<String>? = null
}