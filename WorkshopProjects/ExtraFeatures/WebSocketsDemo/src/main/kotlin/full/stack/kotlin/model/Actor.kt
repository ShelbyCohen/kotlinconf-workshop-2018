package full.stack.kotlin.model

class Actor {
    var birthday: String? = null
    var birthplace: String? = null
    var name: String? = null
    var lastModified: String? = null
    var id: String? = null
    var biography: String? = null
    var version: String? = null
    var profileImageUrl: String? = null
}
