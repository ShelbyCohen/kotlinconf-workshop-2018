package full.stack.kotlin.services

import com.fasterxml.jackson.databind.ObjectMapper
import full.stack.kotlin.clients.Neo4JRestClient
import full.stack.kotlin.model.Movie
import full.stack.kotlin.neo4j.Neo4JResponse
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Component
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration

@Component
class MoviesWebSocketHandler(private val client: Neo4JRestClient) : WebSocketHandler {

    override fun handle(session: WebSocketSession): Mono<Void> {
        val typeRef = object : ParameterizedTypeReference<Neo4JResponse<Movie>>() {}
        val query = "MATCH (m:Movie)<-[:ACTS_IN]-(a:Actor{name:\"%s\"}) RETURN m"
        val fullQuery = String.format(query, "Harrison Ford")

        val movies = client.runCypherQuery(fullQuery, typeRef) ?: Flux.empty()
        val mapper = ObjectMapper()
        val outgoingFlux = movies
                .map { mapper.writeValueAsString(it) }
                .map { session.textMessage(it) }
                .delayElements(Duration.ofMillis(500))

        return session.send(outgoingFlux)
    }
}