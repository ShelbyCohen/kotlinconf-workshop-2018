package demos.spring.reactor.via.gui

interface CellEventListener {
    fun onCellStateChange(x: Int, y: Int, state: Boolean)
    fun onGameStop()
}