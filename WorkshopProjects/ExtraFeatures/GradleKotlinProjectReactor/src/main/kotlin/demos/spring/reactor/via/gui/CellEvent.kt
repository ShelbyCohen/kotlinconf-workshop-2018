package demos.spring.reactor.via.gui

class CellEvent(val x: Int, val y: Int, val alive: Boolean)