package full.stack.kotlin.neo4j

class Neo4JResultRow<T> {
    var row: List<T>? = null
    var meta: List<Neo4JResultMeta>? = null
}