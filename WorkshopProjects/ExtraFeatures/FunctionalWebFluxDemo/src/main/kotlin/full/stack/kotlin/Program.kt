package full.stack.kotlin

import full.stack.kotlin.clients.Neo4JRestClient
import full.stack.kotlin.model.Movie
import full.stack.kotlin.neo4j.Neo4JResponse
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.ipc.netty.http.server.HttpServer

import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.web.reactive.function.BodyInserters.fromPublisher
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.server.RouterFunctions
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.ServerResponse.notFound

import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono
import java.util.Base64.getEncoder

typealias InputFunc = (ServerRequest, Neo4JRestClient) -> Mono<ServerResponse>
typealias OutputFunc = (ServerRequest) -> Mono<ServerResponse>

fun curry(input: InputFunc, client: Neo4JRestClient): OutputFunc {
    return { request: ServerRequest -> input(request, client) }
}

fun someMovies(request: ServerRequest, client: Neo4JRestClient): Mono<ServerResponse> {
    val limit = request.pathVariable("limit")
    val typeRef = object : ParameterizedTypeReference<Neo4JResponse<Movie>>() {}
    val query = "MATCH (m:Movie) RETURN m LIMIT $limit"

    val result = client.runCypherQuery(query, typeRef)
    return if (result != null)
        ok().body(fromPublisher(result, Movie::class.java))
    else
        notFound().build()
}

fun moviesByActor(request: ServerRequest, client: Neo4JRestClient): Mono<ServerResponse> {
    val id = request.pathVariable("id")
    val typeRef = object : ParameterizedTypeReference<Neo4JResponse<Movie>>() {}
    val query = "MATCH (m:Movie)<-[:ACTS_IN]-(a:Actor {id:\"$id\"}) RETURN m"

    val result = client.runCypherQuery(query, typeRef)
    return if (result != null)
        ok().body(fromPublisher(result, Movie::class.java))
    else
        notFound().build()
}

fun client(): WebClient {
    fun buildAuthorization(): String {
        val token = "neo4j:password"
        val encodedToken = getEncoder().encodeToString(token.toByteArray())
        return "Basic $encodedToken"
    }

    return WebClient.builder()
            .baseUrl("http://localhost:7474/db/data/transaction/commit")
            .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .defaultHeader(HttpHeaders.AUTHORIZATION, buildAuthorization())
            .build()
}

fun main(args: Array<String>) {
    val client = Neo4JRestClient(client())

    //Use the Kotlin Routing DSL to set up a RESTful service
    val route = router {
        ("/someMovies" and accept(APPLICATION_JSON)).nest {
            GET("/{limit}", curry(::someMovies, client))
        }
        ("/moviesByActor" and accept(APPLICATION_JSON)).nest {
            GET("/{id}", curry(::moviesByActor, client))
        }
    }

    //Launch an instance of Netty based on our routes
    val httpHandler = RouterFunctions.toHttpHandler(route)
    val adapter = ReactorHttpHandlerAdapter(httpHandler)
    val server = HttpServer.create("localhost", 8080)
    server.startAndAwait(adapter)
}