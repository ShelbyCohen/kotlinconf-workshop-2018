# Welcome to 'Building a Full Stack Web Application in Kotlin'

This repo contains materials for [the Full Stack Kotlin workshop](https://kotlinconf.com/workshops/), to be delivered at [KotlinConf](https://kotlinconf.com) in Amsterdam on 3rd October 2018. The workshop was designed by [Instil Software](https://instil.co/) and will be delivered by [Garth Gilmour](https://twitter.com/GarthGilmour), [Eamonn Boyle](https://twitter.com/BoyleEamonn) and [Richard Gibson](https://twitter.com/rickityg).

The content provided here is as follows:
* The *Documents* folder contains instructions for the workshop (in PDF) and a web page of sample database queries
* The *Neo4JSampleDB* folder contains a graph database of movies and actors that will be used as our data source
* The *WorkshopProjects* folder contains starter projects, sample solutions and additional demos

In addition a copy of the slide deck used during the delivery will be added after the event is complete.

To get started please clone this repo and then proceed as described in *Documents/KotlinConfWorkshopIntroduction.pdf*. If you have any questions in advance of the delivery please email garth.gilmour@instil.co.



